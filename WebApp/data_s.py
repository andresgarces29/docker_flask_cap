#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 29 11:23:52 2021

@author: orc
"""

#importamos del modulo statistics la funcion stdev() 
#la cual se encarga de realizar los calculos de los datos numericos
from statistics import stdev, mean
#definimos una clase llamada Data() la cual se encuentra definida en Test_data_s.py
class Data():
    """
    This class test dataframe iris
    """
    #el metodo _init_ se llama automaticamente al crear la nueva instancia de la clase Data() e iniciamos sus atributos
    #self es el parametro del metodo con el cual accedemos a los metodos y atributos de la instancia los cuales
    #son'df, column="sepal_length", bins=16' estos parametros se llaman de iris.csv
    def __init__(self, df, column="sepal_length", bins=16):
        '''
        Init variables

        Parameters
        ----------
        df : TYPE
            DESCRIPTION.
        column : TYPE, optional
            DESCRIPTION. The default is "sepal_length".
        bins : TYPE, optional
            DESCRIPTION. The default is 16.

        Returns
        -------
        None.

        '''

        self.df = df
        self.column = column
        self.bins = bins
        
#con la subclase resum_data accedemos a todos los atributos en cualquier metodo de la clase usando como parametro self
#realizamos el calculo en las columnas de iris.cvs retornando un resultado.
    def resum_data(self):
        '''
        Calculate basic statistics

        Returns
        -------
        max_f : TYPE
            DESCRIPTION.
        min_f : TYPE
            DESCRIPTION.
        sd_f : TYPE
            DESCRIPTION.
        mean_f : TYPE
            DESCRIPTION.

        '''
        df = self.df
        column = self.column

        max_f = df[column].max()
        min_f = df[column].min()
        sd_f = round(stdev(df[column]), 1)
        mean_f = round(mean(df[column]), 1)


        return max_f, min_f, sd_f, mean_f

#definimos subclase hist_data usando self para acceder a todos los metodos y atributos haciendo tambien uso de plot. para obtener una figura de las estadisticas y calculo con self.
    def hist_data(self):
        '''
        Plot selected column

        Returns
        -------
        None.

        '''
        df = self.df
        column = self.column
        bins = self.bins

        df_col = df[column]
        ax = df_col.plot.hist(bins=bins,
                              alpha=0.7,
                              title=column,
                              legend=True,
                              figsize=(8, 6))
        fig = ax.get_figure()
        fig.savefig("static/plot.jpg")
